#include <stdio.h>
#include "dice.h"

int main() {
	int n;
	scanf("%d", &n);
	
	initializeSeed();
	
	printf("Let's roll the dice: %d\n", rollDice(n));
	
	return 0;
} 
